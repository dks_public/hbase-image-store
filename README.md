# Hbase Image Store

**Use Case**

  Images are collected along with their attributes for Deep Learning Purpose. The attribues are defined in the form of logs and the log also 
  contains the actual path of the image. The data science team demands to access the images in several access patterns. For this use case   Hbase 
  is an pretty good solution.
  
  **Implementation**
  
  As the log files are stored in different directories according to date, it is good to process each different directories by different thread.
  The row key is designed   in such a way that the data can be accessed efficiently for the required access patterns. The logs are processed using   regex and the actual image is converted into byte array which inturn along with their attributes are inserted into
   Hbase table in two seperate column families.