package com.dks;

/** Main class which loops through Log files and images of respective logs and inserts it to HBase **/

import java.io.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class HbaseImg{


    private static final Logger LOG = LoggerFactory.getLogger(HbaseImg.class);

    /**Pattern to match logs*/
    private static final Pattern PATTERN = Pattern.compile("([a-z0-9]{2}),([^a-zA-Z]+),(\\d{2}),([a-z_]*),([\\w]*),(\\d),(\\d),(\\d{2}\\.\\d*),(-\\d{2}\\.\\d*),(\\S*)");
    private static Matcher matcher;
    private static final byte[] FAMILY_1 = "Attributes".getBytes();
    private static final byte[] FAMILY_2 = "ImgFam".getBytes();
    private static final  byte[] []COL_NAMES = {"region".getBytes(),"dateTime".getBytes(),"div-nr".getBytes(),"route".getBytes(),
                                              "routeId".getBytes(),"trackNo".getBytes(),"miles".getBytes(),"lat".getBytes(),
                                              "lng".getBytes(),"Img".getBytes()};
    private static final String DIRECTORY ="< directory name containing logs>";


    public static void main(String[] args)  {

        LOG.info("Initiating Configuration Class");
        Configuration conf = HBaseConfiguration.create();
        conf.addResource("hbase-site.xml");


        /**connection object which will be shared by all the threads*/
        HbaseUtil con = new HbaseUtil(Executors.newFixedThreadPool(10));
       con.processFolder(DIRECTORY,conf);

    }


    /** Method to fetch image as byte array */
    public static byte[] extractBytes(String ImageName) throws IOException {
          System.out.println(ImageName);
        File file = new File(ImageName);
        BufferedImage originalImage = ImageIO.read(file);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(originalImage, "jpg", baos);
        byte[] imageInByte = baos.toByteArray();
        return imageInByte;
    }

    /** Method to inserts image and attribute to Hbase */
    public static  void write(String fileName, Configuration conf){

        LOG.info("Inserting images from file "+fileName);

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName));Connection connection = ConnectionFactory.createConnection(conf);

        ){
            String line = "";
            while ((line = reader.readLine())!= null) {

                matcher = PATTERN.matcher(line);
                if (matcher.matches()) {
                    String region = matcher.group(1);
                    String dateTime = matcher.group(2);
                    String div_nr = matcher.group(3);
                    String route = matcher.group(4);
                    String routeId = matcher.group(5);
                    String trackNo = matcher.group(6);
                    String miles = matcher.group(7);
                    String lat = matcher.group(8);
                    String lng = matcher.group(9);
                    System.out.println(matcher.group(10));

                    byte [] img = extractBytes(matcher.group(10)) ;
                    String rowKey = region+":"+route+":"+dateTime+":"+trackNo+miles;


                    Table table = connection.getTable(TableName.valueOf("dks:dks_image"));
                        Put p = new Put(rowKey.getBytes());
                        p.addColumn(FAMILY_1, COL_NAMES[0], region.getBytes());
                        p.addColumn(FAMILY_1, COL_NAMES[1], dateTime.getBytes());
                        p.addColumn(FAMILY_1, COL_NAMES[2], div_nr.getBytes());
                        p.addColumn(FAMILY_1, COL_NAMES[3], route.getBytes());
                        p.addColumn(FAMILY_1, COL_NAMES[4], routeId.getBytes());
                        p.addColumn(FAMILY_1, COL_NAMES[5], trackNo.getBytes());
                        p.addColumn(FAMILY_1, COL_NAMES[6], miles.getBytes());
                        p.addColumn(FAMILY_1, COL_NAMES[7], lat.getBytes());
                        p.addColumn(FAMILY_1, COL_NAMES[8], lng.getBytes());
                        p.addColumn(FAMILY_2, COL_NAMES[9], img);
                        table.put(p);
                        table.close();

                }
                else {
                    LOG.info("Log :" +line+ "in"+fileName+"is invalid ");
                }

                LOG.info("Inserted images from file "+fileName);

            }
        } catch (IOException e) {
            LOG.warn("Exception in reading Log files"+e.toString());
    }     }

}





