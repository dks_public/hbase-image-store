package com.dks;

/** Helper class for the application for parallelism. So, that each directory and files are processed by seperate threads */
import org.apache.hadoop.conf.Configuration;

import java.io.File;
import java.util.concurrent.ExecutorService;

 class HbaseUtil  {

    private final ExecutorService pool;

    public HbaseUtil(ExecutorService pool){
        this.pool = pool;
    }

    //Method which process the directories and files concurrently
    void processFolder(String inputPath, Configuration conf){
        File inputFolder = new File(inputPath);

        /** Calls itself if the filepath is directory or calls HbaseImg write function to insert the image*/
        for (String filename : inputFolder.list()) {
            String filePath = inputFolder.toPath().resolve(filename).toString();
            if (new File(filePath).isDirectory()) {
                System.out.println(filePath);

                pool.execute(()->processFolder(filePath,conf));

            } else {
                System.out.println(filePath);

                pool.execute(()->HbaseImg.write(filePath, conf));
                }

        }


    }
}



